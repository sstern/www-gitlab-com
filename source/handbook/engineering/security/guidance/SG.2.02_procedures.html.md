---
layout: handbook-page-toc
title: "SG.2.02 - Procedures"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# SG.2.02 - Procedures

## Control Statement

GitLab's key control capabilities are supported by documented procedures that are communicated to authorized personnel.

## Context

All GCF Security Controls are required to have documentation to support and provide insight into how the control is executed. In most cases, specific control pages within the handbook will detail any policy and procedural references that are relevant to a control.

## Scope

The scope of this control is all active control activities in the GCF Security Controls Framework.

## Ownership

* Control Owner: `Security Team`
* Process owner(s):
    * Security Team: `100%`

## Guidance

GitLab's key controls are documented publicly on the company handbook and therefore available to everyone for viewing. There is a comprehensive [overview of GitLab's Security Controls](https://about.gitlab.com/handbook/engineering/security/sec-controls.html) which includes links to [specific control pages](https://about.gitlab.com/handbook/engineering/security/sec-controls.html#list-of-controls-by-family) that are organized by control family. Each control's specific page provides information on the control statement, additional context, the scope of the control, specific ownership group(s), guidance, policy references, additional information as needed, and finally, any relevant compliance framework requirements that the control addresses.

## Additional control information and project tracking

Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Information Security Program control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/878).

### Policy Reference
*  [Security Controls Handbook Page](https://about.gitlab.com/handbook/engineering/security/sec-controls.html)